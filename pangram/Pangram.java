package com.cloudacademy.test;

import java.util.HashSet;
import java.util.Set;

public class Pangram {
    boolean isPangram(String input) {
        String lowerCaseInput = input.toLowerCase();

        // Collect the encountered characters in the input string
        Set<Character> charsFound = new HashSet<>();

        for(int i=0; i<lowerCaseInput.length(); ++i) {
            char ch = lowerCaseInput.charAt(i);

            if(Character.isAlphabetic(ch)) {
                charsFound.add(ch);
            }
        }

        return charsFound.size() == 26;
    }

    public static void main(String[] args) {
        Pangram pangram = new Pangram();
        System.out.println(pangram.isPangram(args[0]));
    }
}
