package com.cloudacademy.test.s3migration;

import java.io.File;
import java.nio.file.Path;
import java.util.Date;

/**
 * Created by gyuri on 6/21/17.
 */
public interface S3Client {
    /**
     * Connect to the S3 service.
     */
    S3Session connect(S3Credentials credentials);

    /**
     * Disconnect from the S3 service.
     */
    void disconnect(S3Session session);

    /**
     * Create brand new bucket in the current session.
     */
    void createBucket(S3Session session, String bucketName);

    /**
     * Upload local file to the specified bucket.
     */
    void uploadFile(S3Session session, Path localFile, String bucket, String key);

    /**
     * Generate publication URL for the specified object in the given bucket.
     *
     *  @param  expireDate no expire if it's not specified
     */
    String generatePublishURL(S3Session session, String bucket, String key, Date expireDate);
}
