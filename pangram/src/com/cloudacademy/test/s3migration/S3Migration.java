package com.cloudacademy.test.s3migration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by gyuri on 6/21/17.
 */
public class S3Migration {
    // TODO inject this component with some DI solution, like Spring autowire
    private S3Client s3Client;

    // TODO configure
    private String sourceDirectory = "/media";

    // TODO configure
    private String targetBucket = "samplebucket";

    // TODO inject with DI or configure with some other configuration management framework
    private S3Credentials credentials;

    // TODO @PostConstruct annotation to initialize wired beans
    public void init() {
        // TODO initalize S3 client if anything is necessary
    }

    public void migrateEverything() {
        S3Session s3Session = null;

        try {
            s3Session = s3Client.connect(credentials);

            // Creates an S3 bucket
            s3Client.createBucket(s3Session, targetBucket);

            // Iterate over files to be migrated
            // TODO go through directories recursively
            S3Session finalS3Session = s3Session;
            
            Files.newDirectoryStream(Paths.get(sourceDirectory)).forEach(p -> {
                // TODO check if this file has been already properly uploaded - use checksums for example

                // Key of the uploaded file on S3
                String key = p.getFileName().toString();

                // Upload file
                s3Client.uploadFile(finalS3Session, p, targetBucket, key);

                // Makes any file public
                s3Client.generatePublishURL(finalS3Session, targetBucket, key, null);

                // Generates the public S3 URL for any uploaded file and then use them for fixing the “Image” fields into the PRODUCTS table.

            });
        } catch (IOException e) {
            // TODO logging
            e.printStackTrace();
        } finally {
            if (s3Session != null) {
                s3Client.disconnect(s3Session);
            }
        }
    }
}
