PROBLEM STATEMENT 
=================

Brad wants to limber up his fingers to increase his typing speed for programming contests.  He read on Quora™ that this is best accomplished by typing the sentence "The quick brown fox jumps over the lazy dog" repeatedly, because it is a pangram. (Pangrams are sentences constructed by using every letter of the alphabet at least once.)
 
After typing the sentence several times, Brad became bored with it. So he started to look for other pangrams.
 
Given a sentence N, complete the function isPangram to tell Brad if it is a pangram or not.
 
Input Format

	Input consists of a string containing N.
 
Output Format

	Return 1, if N is a pangram, otherwise return 0.
 
Constraints
Length of N can be up to 10^3 ( 1 ≤ |N| ≤ 10^3 ) and it may contain spaces, lowercase and uppercase letters. Lowercase and uppercase instances of a letter are considered the same for the purposes of determining if the sentence is a pangram.
 
Sample Input #1

	We promptly judged antique ivory buckles for the next prize    
 
Sample Output #1

	1
 
Explanation #1:
Every letter of the alphabet is represented, so the sentence is a pangram.
 
Sample Input #2

	We promptly judged antique ivory buckles for the prize    
 
Sample Output #2

	0
 
Explanation #2
Eliminating the word "next" removed the only instance of the letter "x", so the sentence is no longer a pangram.
